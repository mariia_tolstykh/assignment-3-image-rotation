#ifndef ERR_H
#define ERR_H

#include <stdint.h>
#include <stdint.h>

enum error {
    OK= 0,
    INCORRECT_ARGS,
    FILE_ERROR,
    INCORRECT_ANGLE,

};

#endif
