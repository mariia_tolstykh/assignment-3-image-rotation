#ifndef IO_H
#define IO_H


#include <stdio.h>

FILE* open_file(const char* str, const char* mode);
int8_t close_file(FILE* file);

#include <stdint.h>
#endif
