#ifndef IMAGE_H
#define IMAGE_H
#include "image.h"

struct image rotate(struct image source);
int8_t valid_angle(uint16_t angle);
int16_t conversion_angle(int16_t angle);
#endif
