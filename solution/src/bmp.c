#include "bmp.h"
#include "bmpio.h"
#include "image.h"
#include <stdio.h>

#define BF_Type 19778
#define BFBIT_Count 24
#define BOff_Bits 54
#define BI_Size 40
#define BI_Planes 1

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header bmpHeader;
    size_t reading = fread(&bmpHeader, sizeof(struct bmp_header), 1, in);
    if (reading == 0) return READ_INVALID_HEADER;
    if (bmpHeader.bfType != BF_Type ) return READ_INVALID_SIGNATURE;
    if (bmpHeader.biBitCount != BFBIT_Count) return READ_INVALID_BITS;
    *img = create_image(bmpHeader.biWidth, bmpHeader.biHeight);
    uint32_t width = img->width;
    for (uint32_t i = 0; i < bmpHeader.biHeight; i++) {
        fread(img->data + i * bmpHeader.biWidth, sizeof(struct pixel), bmpHeader.biWidth, in) ;
        fseek(in, (long) (int32_t)((4 - ((width) * 3) % 4) % 4), SEEK_CUR) ;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    uint32_t width = img->width;
    struct bmp_header bmpHeader = {
    .bfType = BF_Type,
    .biSizeImage = (uint32_t) ((sizeof(struct pixel) * img->width + (int32_t)((4 - ((width) * 3) % 4) % 4)) * img->height),
    .bfileSize = (sizeof(struct bmp_header) + img->width * img->height),
    .bfReserved = 0,
    .bOffBits = BOff_Bits,
    .biSize = BI_Size,
    .biWidth = (uint32_t) img->width,
    .biHeight = (uint32_t) img->height,
    .biPlanes = BI_Planes,
    .biBitCount = BFBIT_Count,
    .biCompression = 0,
    .biXPelsPerMeter = 0,
    .biYPelsPerMeter = 0,
    .biClrUsed = 0,
    .biClrImportant = 0,
    };


    fwrite(&bmpHeader, sizeof(bmpHeader), 1, out);
    for (uint32_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * (img->width), 3, img->width, out) != img->width) {
            return WRITE_ERROR;
        } else {
            size_t residue[] = {0, 0, 0};
            fwrite(residue, 1,(int32_t)((4 - ((width) * 3) % 4) % 4), out);
        }
    }
    return WRITE_OK;
}
