#include "image.h"
#include <stdint.h>
#include <stdlib.h>

struct image create_image(uint32_t const width, uint32_t const height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)
    };
}

void delete_image(struct image *image) {
    if (image->data == NULL) {
        return;
    }
    free(image->data);
    image->height = 0;
    image->width = 0;
    image->data = NULL;
}
