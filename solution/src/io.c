#include <stdint.h>
#include <stdio.h>

FILE* open_file(const char* str, const char* mode){
    return fopen(str, mode);
}


int8_t close_file(FILE* file){
    if(!file) return -1;
    return fclose(file) == 0 ? 0 : -1;
}
