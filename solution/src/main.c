#include <stdio.h>
#include <stdlib.h>


#include "image.h"
#include "bmpio.h"
#include "error.h"
#include "io.h"
#include "rotation.h"




int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "Only three arguments: <original image>, <transformed image> and <the_desired_angle>\n");
        return INCORRECT_ARGS;
    }
    FILE *in = open_file( argv[1], "rb" );

    if (!in) {
        fprintf(stderr, "Input file not found\n");
        return FILE_ERROR;
    }

    struct image image ;
    uint32_t status = from_bmp(in, &image);
    if (status != READ_OK) {
        fprintf(stderr, "The input file cannot be read\n");
        if (close_file(in) == -1) fprintf(stderr, "The file cannot be closed\n");
        return FILE_ERROR;
    }
    if (close_file(in) == -1) fprintf(stderr, "The file cannot be closed\n");
    char* c;
    int64_t angle = ((int64_t) strtol(argv[3], &c, 10) + 360) % 360;

    int16_t the_desired_angle = conversion_angle((int16_t)angle);
    if (!valid_angle(the_desired_angle)) {
        delete_image(&image);
        fprintf(stderr, "Possible range: 0, 90, 180, 270\n");
        return INCORRECT_ANGLE;
    }

    uint16_t rotation = ((4 - the_desired_angle / 90) % 4);
    for (uint16_t i = 0; i < rotation; i++) {
        struct image rotated = rotate(image);
        delete_image(&image);
        image = rotated;
    }

    FILE *out = open_file(argv[2], "wb");
    if (!out) {
        fprintf(stderr, "Output file not found\n");
        delete_image(&image);
        return FILE_ERROR;
    }
    uint32_t toBmp = to_bmp(out, &image);
    if (toBmp != WRITE_OK) {
        fprintf(stderr, "The input file cannot be write\n");
        delete_image(&image);
        if (close_file(out) == -1) fprintf(stderr, "The file cannot be closed\n");
        return FILE_ERROR;
    }

    delete_image(&image);
    fprintf(stdout, "The image is rotated\n");
    return OK;
}
