#include "rotation.h"
#include "image.h"


struct image rotate(struct image const source) {
    struct image rotatedImage = create_image(source.height, source.width);
    for (uint32_t i = 0; i < source.height; i++) {
        for (uint32_t j = 0; j < source.width; j++) {
            if (source.data)
            rotatedImage.data[source.height * (j + 1) - i - 1] = source.data[source.width * i + j];
        }
    }
    return rotatedImage;
}
int8_t valid_angle(uint16_t angle) {
    if (angle % 90 != 0) return 0;
    return 1;
}
int16_t conversion_angle(int16_t angle){
    return (int16_t)(((angle) + 360) % 360);
}

